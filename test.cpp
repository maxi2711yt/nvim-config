#include <iostream>

class Text {
public:
  Text(char *text) { _text = text; }

  [[nodiscard]] char *get_text() const noexcept { return _text; }
  void set_text(char *text) { _text = text; }

private:
  char *_text;
};

int main() {
  Text t("Das ist mein Text");
  std::cout << t.get_text() << std::endl;
  return 0;
}
