source $HOME/.config/nvim/pluggins/pluggins.vim
source $HOME/.config/nvim/keys/keys.vim
source $HOME/.config/nvim/general/general.vim
source $HOME/.config/nvim/themes/themes.vim

lua require('treesitter')

"luafile $HOME/.config/nvim/plug-config/compe-config.lua

"autocmd BufWritePre *.c* lua vim.lsp.buf.formatting_sync(nil, 100)
"autocmd BufWritePre *.h* lua vim.lsp.buf.formatting_sync(nil, 100)


"let g:cpp_class_scope_highlight = 1
"let g:cpp_member_variable_highlight = 1
"let g:cpp_class_decl_highlight = 1

set syntax=1
