call plug#begin('~/.config/nvim/autoload/plugged')

" Better Syntax support
" Plug 'sheerun/vim-polyglot'

" File Explorer
" Plug 'scrooloose/NERDTree'

" Auto pairs for ( [ {
" Plug 'jiangmiao/auto-pairs'

" Themes
" Plug 'tomasiser/vim-code-dark'
" Plug 'morhetz/gruvbox'
Plug 'Mofiqul/vscode.nvim'


Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'p00f/nvim-ts-rainbow'
" Deoplete
" Plug 'Shougo/deoplete.nvim', {'do':':UpdateRemotePlugins'}
" let g:deoplete#enable_at_startup = 1

" Plug 'neovim/nvim-lspconfig'
" Plug 'hrsh7th/nvim-compe'
" Plug 'jackguo380/vim-lsp-cxx-highlight'

call plug#end()


let g:vscode_style = "dark"

colorscheme vscode
